import { CacheKey, Controller, Get, UseInterceptors } from '@nestjs/common';
import { AppService } from './app.service';
import { CacheManagerInterceptor } from './interceptor/cache-manager.interceptor';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  

  @Get()
  @CacheKey('greet_key')
  @UseInterceptors(CacheManagerInterceptor)
  async getHello(): Promise<string> {
    return this.appService.getHello();
  }

  @Get('question')
  @CacheKey('question_key')
  @UseInterceptors(CacheManagerInterceptor)
  async questionWorld(): Promise<string> {
    return 'hiiiiii';  
  }
}
